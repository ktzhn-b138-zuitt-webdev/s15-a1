let x = parseInt(prompt("Enter the first number:"));
console.log(x);

let y = parseInt(prompt("Enter the second number:"));
console.log(y);

let sum =  x + y;
console.log("The initial sum.");
console.log(sum);

/*3. Prompt the user for 2 numbers and perform different arithmetic operations based on the total of the two numbers:
- Less than 10, add the numbers
- 10 - 20, subtract the numbers
- 21 - 30 multiply the numbers
- Greater than or equal to 31, divide the numbers
*/
let operationResult;
function operation(x, y){
	if(sum < 10){
		return (x + y);
	}
	else if(sum >=10 && sum <= 20){
		return (x - y);
	}
	else if(sum >=21 && sum <= 30){
		return (x * y);
	}
	else if (sum >= 31){
		return (x / y);
	}
	
}
console.log("The result after arithmetic operation.");
console.log(operation(x, y));

/*Total above or equal 10 or less than 9*/

let warnAlert = (sum >= 10) ? window.alert("Total is above or equal to 10.") : console.warn("The total is 9 or less btw."); 


/*
5. Prompt the user for their name and age and print out different alert messages based on the user input:
- If the name OR age is blank/null, print the message are you a time traveler?
- If the name AND age is not blank, print the message with the user’s name and age.*/

let name = prompt("What is your name?");
let age = parseInt(prompt("How old are you?"));

console.log (name);
console.log (age);

if (name !== "" && age !== NaN){
	window.alert("Hello " + name + " you are " + age + " years old.");
}

else {
	window.alert("Are you a time traveler?");
} 



function isLegalAge() {
	if(age <= 17){
		window.alert ("You are not allowed here.");
	}
	else if(age >= 18) {
		window.alert ("You are of legal age.");
	}
}

isLegalAge();



switch (age){
	case 65:
		window.alert("We thank you for your contribution to society.");
		break;
	case 21:
		window.alert("You are now part of the adult society.");
		break;
	case 18:
		window.alert("You are now allowed to party");
		break;
	default: 
		window.alert("Are you sure you're not an alien?");
		break;
}

/*
8. Create a try catch finally statement to force an error, print the error message as a warning and use the function isLegalAge to print alert another message.
*/

function trycatchy(){
	try{
		aleart(isLegalAge());
	}
	catch (error) {
		console.log(typeof error);
	}
	finally {
		alert(isLegalAge());
	}
}